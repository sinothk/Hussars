package com.sinothk.amap.choose.inters;

import com.sinothk.amap.choose.bean.LocationBean;

public interface LocationCallback {
    void onLocation(LocationBean locationBean);
}
