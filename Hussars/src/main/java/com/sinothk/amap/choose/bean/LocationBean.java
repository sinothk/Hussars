package com.sinothk.amap.choose.bean;

import android.content.Context;

import com.amap.api.location.AMapLocation;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.geocoder.GeocodeResult;
import com.amap.api.services.geocoder.GeocodeSearch;
import com.amap.api.services.geocoder.RegeocodeQuery;
import com.amap.api.services.geocoder.RegeocodeResult;
import com.amap.api.services.geocoder.StreetNumber;
import com.sinothk.amap.choose.inters.LocationCallback;

public class LocationBean {
    private int errorCode;

    private double latitude;
    private double longitude;
    private String aoiName;
    private int locationType;
    private String country;
    private String adCode;
    private String cityCode;
    private String address;
    private float accuracy;
    private String streetNum;
    private String street;
    private String district;
    private String city;
    private String province;

//    {
//        "adCode": "520115",
//            "aois": [],
//        "building": "",
//            "businessAreas": [],
//        "city": "贵阳市",
//            "cityCode": "0851",
//            "country": "中国",
//            "countryCode": "",
//            "crossroads": [],
//        "district": "观山湖区",
//            "formatAddress": "贵州省贵阳市观山湖区金岭社区服务中心都匀路6号俊发滨湖俊园",
//            "neighborhood": "",
//            "pois": [],
//        "province": "贵州省",
//            "roads": [],
//        "streetNumber": {
//        "direction": "东",
//                "distance": 14.1944,
//                "latLonPoint": {
//            "latitude": 26.615969,
//                    "longitude": 106.650036
//        },
//        "number": "6号",
//                "street": "都匀路"
//    },
//        "towncode": "520115407000",
//            "township": "金岭社区服务中心"
//    }

//    public LocationBean() {
//    }


    public LocationBean(int errorCode) {
        this.errorCode = errorCode;
    }

    public LocationBean(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAoiName() {
        return aoiName;
    }

    public void setAoiName(String aoiName) {
        this.aoiName = aoiName;
    }

    public int getLocationType() {
        return locationType;
    }

    public void setLocationType(int locationType) {
        this.locationType = locationType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAdCode() {
        return adCode;
    }

    public void setAdCode(String adCode) {
        this.adCode = adCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public String getStreetNum() {
        return streetNum;
    }

    public void setStreetNum(String streetNum) {
        this.streetNum = streetNum;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public static void getInfo(Context mContext, AMapLocation aMapLocation, LocationCallback locationListener) throws AMapException {

        if (aMapLocation != null && aMapLocation.getErrorCode() == 0) {
            LocationBean locationBean = new LocationBean(aMapLocation.getLatitude(), aMapLocation.getLongitude());
            locationBean.setErrorCode(aMapLocation.getErrorCode());

            if ("lbs".equals(aMapLocation.getProvider())) {
                locationBean.setLocationType(aMapLocation.getLocationType());
                locationBean.setCountry(aMapLocation.getCountry());
                locationBean.setProvince(aMapLocation.getProvince());
                locationBean.setCity(aMapLocation.getCity());
                locationBean.setDistrict(aMapLocation.getDistrict());
                locationBean.setStreet(aMapLocation.getStreet());
                locationBean.setStreetNum(aMapLocation.getStreetNum());
                locationBean.setAccuracy(aMapLocation.getAccuracy());
                locationBean.setAddress(aMapLocation.getAddress());
                locationBean.setCityCode(aMapLocation.getCityCode());
                locationBean.setAdCode(aMapLocation.getAdCode()); //地区编码
                locationBean.setAoiName(aMapLocation.getAoiName()); //获取当前定位点的AOI信息

                locationListener.onLocation(locationBean);
            } else {
                GeocodeSearch geocoderSearch = new GeocodeSearch(mContext);
                geocoderSearch.setOnGeocodeSearchListener(new GeocodeSearch.OnGeocodeSearchListener() {
                    @Override
                    public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {
                        if (regeocodeResult != null && regeocodeResult.getRegeocodeAddress() != null) {
                            locationBean.setLocationType(0);
                            locationBean.setCountry(regeocodeResult.getRegeocodeAddress().getCountry());
                            locationBean.setProvince(regeocodeResult.getRegeocodeAddress().getProvince());
                            locationBean.setCity(regeocodeResult.getRegeocodeAddress().getCity());
                            locationBean.setDistrict(regeocodeResult.getRegeocodeAddress().getDistrict());
                            locationBean.setAddress(regeocodeResult.getRegeocodeAddress().getFormatAddress());
                            locationBean.setCityCode(regeocodeResult.getRegeocodeAddress().getCityCode());
                            locationBean.setAdCode(regeocodeResult.getRegeocodeAddress().getAdCode()); //地区编码

                            if (regeocodeResult.getRegeocodeAddress().getStreetNumber() != null) {
                                StreetNumber streetNumber = regeocodeResult.getRegeocodeAddress().getStreetNumber();
                                locationBean.setStreet(streetNumber.getStreet());
                                locationBean.setStreetNum(streetNumber.getNumber());
                                locationBean.setAccuracy(streetNumber.getDistance());
                            } else {
                                locationBean.setStreet(regeocodeResult.getRegeocodeAddress().getTownship());
                                locationBean.setAccuracy(50);
                            }
//                            locationBean.setAoiName(regeocodeResult.getRegeocodeAddress().getAoiName()); //获取当前定位点的AOI信息
                        }

                        locationListener.onLocation(locationBean);
                    }

                    @Override
                    public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {

                    }
                });

                // 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
                LatLonPoint latLonPoint = new LatLonPoint(aMapLocation.getLatitude(), aMapLocation.getLongitude());
                RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200, GeocodeSearch.AMAP);
                geocoderSearch.getFromLocationAsyn(query);
            }
        }else{
            locationListener.onLocation(new LocationBean(aMapLocation.getErrorCode()));
        }
    }
}
