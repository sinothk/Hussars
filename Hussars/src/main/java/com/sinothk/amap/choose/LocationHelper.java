package com.sinothk.amap.choose;

import android.annotation.SuppressLint;
import android.content.Context;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.services.core.AMapException;
import com.sinothk.amap.choose.bean.LocationBean;
import com.sinothk.amap.choose.inters.LocationCallback;

public class LocationHelper {

    public enum LocationMode {
        LOC_MODE_ALL, LOC_MODE_NET, LOC_MODE_DEV
    }

//    public static int LOC_MODE_ALL = 0;
//    public static int LOC_MODE_NET = 0;
//    public static int LOC_MODE_DEV = 0;

    private static LocationHelper locationHelper;

    public static LocationHelper obj() {
        if (locationHelper == null) {
            synchronized (LocationHelper.class) {
                if (null == locationHelper) {
                    locationHelper = new LocationHelper();
                }
            }
        }
        return locationHelper;
    }

//    /**
//     * 使用 singleLocation 代替
//     *
//     * @param mContext
//     * @param locationListener
//     */
//    @Deprecated
//    public void startLocation(Context mContext, AMapLocationListener locationListener) throws Exception {
//        AMapLocationClient mLocationClient = new AMapLocationClient(mContext);
//        //初始化定位参数
//        AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
//        //设置返回地址信息，默认为true
//        mLocationOption.setNeedAddress(true);
//        //设置定位监听
//        mLocationClient.setLocationListener(locationListener);
//        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
//        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
//
//        // 设置定位间隔,单位毫秒,默认为2000ms
//        // mLocationOption.setInterval(2000);
//
//        //获取一次定位结果：
//        //该方法默认为false。
//        mLocationOption.setOnceLocation(true);
//        //获取最近3s内精度最高的一次定位结果：
//        //设置setOnceLocationLatest(boolean b)接口为true，启动定位时SDK会返回最近3s内精度最高的一次定位结果。如果设置其为true，setOnceLocation(boolean b)接口也会被设置为true，反之不会，默认为false。
//        mLocationOption.setOnceLocationLatest(true);
//
//        //设置定位参数
//        mLocationClient.setLocationOption(mLocationOption);
//        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
//        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
//        // 在定位结束后，在合适的生命周期调用onDestroy()方法
//        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
//        //启动定位
//        mLocationClient.startLocation();
//
////        @Override
////        public void onLocationChanged (AMapLocation amapLocation){
////            if (amapLocation != null) {
////                if (amapLocation.getErrorCode() == 0) {
////                    //定位成功回调信息，设置相关消息
////                    amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
////                    amapLocation.getLatitude();//获取纬度
////                    amapLocation.getLongitude();//获取经度
////                    amapLocation.getAccuracy();//获取精度信息
////                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
////                    Date date = new Date(amapLocation.getTime());
////                    df.format(date);//定位时间
////                    amapLocation.getAddress();//地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
////                    amapLocation.getCountry();//国家信息
////                    amapLocation.getProvince();//省信息
////                    amapLocation.getCity();//城市信息
////                    amapLocation.getDistrict();//城区信息
////                    amapLocation.getStreet();//街道信息
////                    amapLocation.getStreetNum();//街道门牌号信息
////                    amapLocation.getCityCode();//城市编码
////                    amapLocation.getAdCode();//地区编码
////                    amapLocation.getAOIName();//获取当前定位点的AOI信息
////                } else {
////                    //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
////                    Log.e("AmapError", "location Error, ErrCode:"
////                            + amapLocation.getErrorCode() + ", errInfo:"
////                            + amapLocation.getErrorInfo());
////                }
////            }
////        }
//    }

    //声明mLocationOption对象
    public AMapLocationClientOption mLocationOption = null;
    //声明mlocationClient对象
    @SuppressLint("StaticFieldLeak")
    public AMapLocationClient mLocationClient;

//    /**
//     * 使用 recycleLocation 替换
//     *
//     * @param mContext
//     * @param interval
//     * @param locationListener
//     */
//    @Deprecated
//    public void startLocationRecycle(Context mContext, long interval, AMapLocationListener locationListener) throws Exception {
//        if (mLocationClient != null) {
//            mLocationClient.stopLocation();
//            mLocationClient.onDestroy();
//            mLocationOption = null;
//            mLocationClient = null;
//        }
//
//        mLocationClient = new AMapLocationClient(mContext);
//        //初始化定位参数
//        mLocationOption = new AMapLocationClientOption();
//        //设置返回地址信息，默认为true
//        mLocationOption.setNeedAddress(true);
//        //设置定位监听
//        mLocationClient.setLocationListener(locationListener);
//        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
//        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
//        //设置定位间隔,单位毫秒,默认为2000ms
//        mLocationOption.setInterval(interval < 1000 ? 1000 : interval);
//        //设置定位参数
//        mLocationClient.setLocationOption(mLocationOption);
//        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
//        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
//        // 在定位结束后，在合适的生命周期调用onDestroy()方法
//        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
//        //启动定位
//        mLocationClient.startLocation();
//    }

    public void onDestroy() {
        if (mLocationClient != null) {
            mLocationClient.stopLocation();
            mLocationClient.onDestroy();
            mLocationOption = null;
            mLocationClient = null;
        }
    }

//    public void startLocationRecycle(Context mContext, long interval, AMapLocationClientOption.AMapLocationMode locationMode, LocationCallback locationListener) {
//        if (mLocationClient != null) {
//            mLocationClient.stopLocation();
//            mLocationClient.onDestroy();
//            mLocationOption = null;
//            mLocationClient = null;
//        }
//
//        mLocationClient = new AMapLocationClient(mContext);
//        //初始化定位参数
//        mLocationOption = new AMapLocationClientOption();
//        //设置返回地址信息，默认为true
//        mLocationOption.setNeedAddress(true);
//        //设置定位监听
//        mLocationClient.setLocationListener(locationListener);
//        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
//        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
//        //设置定位间隔,单位毫秒,默认为2000ms
//        mLocationOption.setInterval(interval < 1000 ? 1000 : interval);
//        //设置定位参数
//        mLocationClient.setLocationOption(mLocationOption);
//        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
//        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
//        // 在定位结束后，在合适的生命周期调用onDestroy()方法
//        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
//        //启动定位
//        mLocationClient.startLocation();
//    }


//    private void getAddress(Context mContext) {
//
//        GeocodeSearch geocoderSearch = new GeocodeSearch(mContext);
//        geocoderSearch.setOnGeocodeSearchListener(new GeocodeSearch.OnGeocodeSearchListener() {
//            @Override
//            public void onRegeocodeSearched(RegeocodeResult regeocodeResult, int i) {
//
//            }
//
//            @Override
//            public void onGeocodeSearched(GeocodeResult geocodeResult, int i) {
//
//            }
//        });
//
//        // 第一个参数表示一个Latlng，第二参数表示范围多少米，第三个参数表示是火系坐标系还是GPS原生坐标系
//        LatLonPoint
//        RegeocodeQuery query = new RegeocodeQuery(latLonPoint, 200, GeocodeSearch.AMAP);
//        geocoderSearch.getFromLocationAsyn(query);
//    }

    public void singleLocation(Context mContext, LocationCallback locationListener) throws Exception {
        singleLocation(mContext, false, LocationMode.LOC_MODE_ALL, locationListener);
    }

    public void singleLocationWhitNeedAddress(Context mContext, LocationCallback locationListener) throws Exception {
        singleLocation(mContext, true, LocationMode.LOC_MODE_ALL, locationListener);
    }

    public void singleLocation(Context mContext, boolean needAddress, LocationMode locationMode, LocationCallback locationListener) throws Exception {
        AMapLocationClient mLocationClient = new AMapLocationClient(mContext);
        //初始化定位参数
        AMapLocationClientOption mLocationOption = new AMapLocationClientOption();
        //设置返回地址信息，默认为true
        mLocationOption.setNeedAddress(needAddress);
        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        if (locationMode == LocationMode.LOC_MODE_NET) {
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        } else if (locationMode == LocationMode.LOC_MODE_DEV) {
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Device_Sensors);
        } else {
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        }

        // 设置定位间隔,单位毫秒,默认为2000ms
        // mLocationOption.setInterval(2000);

        //获取一次定位结果：
        //该方法默认为false。
        mLocationOption.setOnceLocation(true);
        //获取最近3s内精度最高的一次定位结果：
        //设置setOnceLocationLatest(boolean b)接口为true，启动定位时SDK会返回最近3s内精度最高的一次定位结果。如果设置其为true，setOnceLocation(boolean b)接口也会被设置为true，反之不会，默认为false。
        mLocationOption.setOnceLocationLatest(true);

        //设置定位参数
        mLocationClient.setLocationOption(mLocationOption);
        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
        // 在定位结束后，在合适的生命周期调用onDestroy()方法
        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除

        //设置定位监听
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                try {
                    LocationBean.getInfo(mContext, aMapLocation, locationListener);
                } catch (AMapException e) {
                    e.printStackTrace();
                }
            }
        });
        //启动定位
        mLocationClient.startLocation();

//        @Override
//        public void onLocationChanged (AMapLocation amapLocation){
//            if (amapLocation != null) {
//                if (amapLocation.getErrorCode() == 0) {
//                    //定位成功回调信息，设置相关消息
//                    amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
//                    amapLocation.getLatitude();//获取纬度
//                    amapLocation.getLongitude();//获取经度
//                    amapLocation.getAccuracy();//获取精度信息
//                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                    Date date = new Date(amapLocation.getTime());
//                    df.format(date);//定位时间
//                    amapLocation.getAddress();//地址，如果option中设置isNeedAddress为false，则没有此结果，网络定位结果中会有地址信息，GPS定位不返回地址信息。
//                    amapLocation.getCountry();//国家信息
//                    amapLocation.getProvince();//省信息
//                    amapLocation.getCity();//城市信息
//                    amapLocation.getDistrict();//城区信息
//                    amapLocation.getStreet();//街道信息
//                    amapLocation.getStreetNum();//街道门牌号信息
//                    amapLocation.getCityCode();//城市编码
//                    amapLocation.getAdCode();//地区编码
//                    amapLocation.getAOIName();//获取当前定位点的AOI信息
//                } else {
//                    //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
//                    Log.e("AmapError", "location Error, ErrCode:"
//                            + amapLocation.getErrorCode() + ", errInfo:"
//                            + amapLocation.getErrorInfo());
//                }
//            }
//        }
    }

    public void recycleLocation(Context mContext, long interval, LocationCallback locationListener) throws Exception {
        recycleLocation(mContext, interval, false, LocationMode.LOC_MODE_ALL, locationListener);
    }

    public void recycleLocationWhitNeedAddress(Context mContext, long interval, LocationCallback locationListener) throws Exception {
        recycleLocation(mContext, interval, true, LocationMode.LOC_MODE_ALL, locationListener);
    }

    public void recycleLocation(Context mContext, long interval, boolean needAddress, LocationMode locationMode, LocationCallback locationListener) throws Exception {
        if (mLocationClient != null) {
            mLocationClient.stopLocation();
            mLocationClient.onDestroy();
            mLocationOption = null;
            mLocationClient = null;
        }

        mLocationClient = new AMapLocationClient(mContext);
        //初始化定位参数
        mLocationOption = new AMapLocationClientOption();
        //设置返回地址信息，默认为true
        mLocationOption.setNeedAddress(needAddress);

        //设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        if (locationMode == LocationMode.LOC_MODE_NET) {
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Battery_Saving);
        } else if (locationMode == LocationMode.LOC_MODE_DEV) {
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Device_Sensors);
        } else {
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        }
        //设置定位间隔,单位毫秒,默认为2000ms
        mLocationOption.setInterval(interval < 1000 ? 1000 : interval);
        //设置定位参数
        mLocationClient.setLocationOption(mLocationOption);
        // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        // 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
        // 在定位结束后，在合适的生命周期调用onDestroy()方法
        // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除

        //设置定位监听
        mLocationClient.setLocationListener(new AMapLocationListener() {
            @Override
            public void onLocationChanged(AMapLocation aMapLocation) {
                try {
                    LocationBean.getInfo(mContext, aMapLocation, locationListener);
                } catch (AMapException e) {
                    e.printStackTrace();
                }
            }
        });
        //启动定位
        mLocationClient.startLocation();
    }
}
