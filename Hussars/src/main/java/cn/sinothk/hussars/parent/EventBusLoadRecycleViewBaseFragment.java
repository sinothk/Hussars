package cn.sinothk.hussars.parent;

import android.content.Context;

import androidx.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;

public abstract class EventBusLoadRecycleViewBaseFragment<T> extends LoadRecycleViewBaseFragment<T> {

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public fun eventBusCallback(result: AppResultData<AppPageData<LawCaseEntity>>) {
//
//        if ("getHomeLawCasePageData" != result.eventType) {
//            return
//        }
//
//        stopLoading(loadingRecyclerView, loadType)
//
//        when (result.code) {
//            AppResultData.SUCCESS -> {
//                if (result.data != null) {
//                    setPageData(result.data)
//                } else {
//                    loadingTipView.showError("数据异常")
//                }
//            }
//            AppResultData.TOKEN_OVERDUE -> {
//                loadingTipView.showError("Token过期，请重新登录")
//            }
//            else -> {
//                loadingTipView.showError(result.msg)
//            }
//        }
//    }
//
//    private fun setPageData(appPageData: AppPageData<LawCaseEntity>) {
//        if (loadType == LoadType.REFRESH) {
//            if (appPageData.records.size == 0) {
//                loadingTipView.showEmpty("暂无数据", R.drawable.icon_no_data_gray)
//                loadingRecyclerView.setLoadingMoreEnabled(false)
//            } else {
//                adapter!!.setData(appPageData.records!!)
//                loadingTipView.showContent()
//            }
//        } else {
//            if (appPageData.records.size == 0) {
//                tip("没有更多数据了")
//            } else {
//                adapter!!.updateData(appPageData.records!!)
//                loadingTipView.showContent()
//            }
//        }
//
//        // 设置：通用
//        if (appPageData.current < appPageData.pages) {
//            loadingRecyclerView.setLoadingMoreEnabled(true)
//        } else {
//            loadingRecyclerView.setLoadingMoreEnabled(false)
//            loadingRecyclerView.setNoMore(true)
//        }
//    }
}
