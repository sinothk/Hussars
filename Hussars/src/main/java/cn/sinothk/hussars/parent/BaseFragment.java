package cn.sinothk.hussars.parent;

import androidx.fragment.app.Fragment;

import com.sinothk.android.utils.XUtils;
import com.sinothk.dialog.loading.LoadingDialog;

public abstract class BaseFragment extends Fragment {

    private LoadingDialog loadingDialog;

    public void tip(String msg) {
        XUtils.toast().show(msg);
    }

    public void showLoadingDialog(String msg) {
        loadingDialog = new LoadingDialog(getActivity());
        loadingDialog.show(false, msg);
    }

    public void hideLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }
}
