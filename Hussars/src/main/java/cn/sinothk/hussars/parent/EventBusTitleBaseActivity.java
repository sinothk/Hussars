package cn.sinothk.hussars.parent;

import org.greenrobot.eventbus.EventBus;

public abstract class EventBusTitleBaseActivity extends TitleBaseActivity {

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public fun eventBusCallback(result: ResultInfo<JuBaoChaXunBean>) {
//        if (result.eventType != "getReplyByCode") {
//            return
//        }
//
//        hideLoadingDialog()
//
//        when (result.code) {
//            ResultInfo.SUCCESS -> {
//                if (result.data != null) {
//                    XUtils.intent().openActivity(this, JuBao12380ChaXunResultActivity::class.java)
//                        .putSerializableExtra("result", result.data)
//                            .start()
//                } else {
//                    XUtils.toast().show("查无结果")
//                }
//            }
//            ResultInfo.TOKEN_OVERDUE -> {
//            }
//            else -> {
//                XUtils.toast().show(result.msg)
//            }
//        }
//    }


}
