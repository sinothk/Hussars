package cn.sinothk.hussars.parent;

import android.content.Context;

import androidx.annotation.NonNull;

import org.greenrobot.eventbus.EventBus;

public class EventBusBaseFragment extends BaseFragment {

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    //    @Subscribe(threadMode = ThreadMode.MAIN)
//    public fun eventBusCallback(result: ResultInfo<JuBaoChaXunBean>) {
//        if (result.eventType != "getReplyByCode") {
//            return
//        }
//
//        hideLoadingDialog()
//
//        when (result.code) {
//            ResultInfo.SUCCESS -> {
//                if (result.data != null) {
//                    XUtils.intent().openActivity(this, JuBao12380ChaXunResultActivity::class.java)
//                        .putSerializableExtra("result", result.data)
//                            .start()
//                } else {
//                    XUtils.toast().show("查无结果")
//                }
//            }
//            ResultInfo.TOKEN_OVERDUE -> {
//            }
//            else -> {
//                XUtils.toast().show(result.msg)
//            }
//        }
//    }
}
