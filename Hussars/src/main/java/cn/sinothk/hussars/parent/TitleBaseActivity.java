package cn.sinothk.hussars.parent;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.gyf.immersionbar.BarHide;
import com.gyf.immersionbar.ImmersionBar;
import com.sinothk.android.utils.XUtils;
import com.sinothk.android.views.TitleBarView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.sinothk.hussars.R;

//ImmersionBar.with(this)
//.transparentStatusBar()  //透明状态栏，不写默认透明色
//.transparentNavigationBar()  //透明导航栏，不写默认黑色(设置此方法，fullScreen()方法自动为true)
//.transparentBar()             //透明状态栏和导航栏，不写默认状态栏为透明色，导航栏为黑色（设置此方法，fullScreen()方法自动为true）
//.statusBarColor(R.color.colorPrimary)     //状态栏颜色，不写默认透明色
//.navigationBarColor(R.color.colorPrimary) //导航栏颜色，不写默认黑色
//.barColor(R.color.colorPrimary)  //同时自定义状态栏和导航栏颜色，不写默认状态栏为透明色，导航栏为黑色
//.statusBarAlpha(0.3f)  //状态栏透明度，不写默认0.0f
//.navigationBarAlpha(0.4f)  //导航栏透明度，不写默认0.0F
//.barAlpha(0.3f)  //状态栏和导航栏透明度，不写默认0.0f
//.statusBarDarkFont(true)   //状态栏字体是深色，不写默认为亮色
//.flymeOSStatusBarFontColor(R.color.btn3)  //修改flyme OS状态栏字体颜色
//.fullScreen(true)      //有导航栏的情况下，activity全屏显示，也就是activity最下面被导航栏覆盖，不写默认非全屏
//.hideBar(BarHide.FLAG_HIDE_BAR)  //隐藏状态栏或导航栏或两者，不写默认不隐藏
//.addViewSupportTransformColor(toolbar)  //设置支持view变色，可以添加多个view，不指定颜色，默认和状态栏同色，还有两个重载方法
//.titleBar(view)    //解决状态栏和布局重叠问题，任选其一
//.titleBarMarginTop(view)     //解决状态栏和布局重叠问题，任选其一
//.statusBarView(view)  //解决状态栏和布局重叠问题，任选其一
//.fitsSystemWindows(true)    //解决状态栏和布局重叠问题，任选其一，默认为false，当为true时一定要指定statusBarColor()，不然状态栏为透明色
//.supportActionBar(true) //支持ActionBar使用
//.statusBarColorTransform(R.color.orange)  //状态栏变色后的颜色
//.navigationBarColorTransform(R.color.orange) //导航栏变色后的颜色
//.barColorTransform(R.color.orange)  //状态栏和导航栏变色后的颜色
//.removeSupportView(toolbar)  //移除指定view支持
//.removeSupportAllView() //移除全部view支持
//.navigationBarEnable(true)   //是否可以修改导航栏颜色，默认为true
//.navigationBarWithKitkatEnable(true)  //是否可以修改安卓4.4和emui3.1手机导航栏颜色，默认为true
//.fixMarginAtBottom(true)   //已过时，当xml里使用android:fitsSystemWindows="true"属性时,解决4.4和emui3.1手机底部有时会出现多余空白的问题，默认为false，非必须
//.addTag("tag")  //给以上设置的参数打标记
//.getTag("tag")  //根据tag获得沉浸式参数
//.reset()  //重置所以沉浸式参数
//.keyboardEnable(true)  //解决软键盘与底部输入框冲突问题，默认为false，还有一个重载方法，可以指定软键盘mode
//.init();  //必须调用方可沉浸式

public abstract class TitleBaseActivity extends BaseActivity {

    protected void initTitleBarDarkStyle() {
        ImmersionBar.with(this)
                .statusBarDarkFont(true)
                .navigationBarColor(cn.sinothk.hussars.R.color.app_navi_dark_color)
                .navigationBarColorTransform(cn.sinothk.hussars.R.color.app_navi_dark_color)
                .init();
    }

    protected void initTitleBarLightStyle() {
        ImmersionBar.with(this)
                .statusBarDarkFont(false)
                .navigationBarColor(cn.sinothk.hussars.R.color.app_navi_color)
                .navigationBarColorTransform(cn.sinothk.hussars.R.color.app_navi_color)
                .init();
    }

    protected void initTitleBarFullStyle() {
        ImmersionBar.with(this)
                .fullScreen(true)
                .hideBar(BarHide.FLAG_HIDE_BAR)
                .navigationBarColor(R.color.app_navi_color)
                .navigationBarColorTransform(R.color.app_navi_color)
                .init();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        initTitleBarLightStyle();
        XUtils.page().addActivity(this);
    }


    protected abstract int getLayoutResId();

    /**
     * 修改右边图标
     *
     * @param imgResId
     */
    public void setViewTitleRightIcon(int imgResId) {
        titleBarView.setRight1Icon(imgResId);
    }

//    public void dealKeybord(EditText editText) {
//        if (ModelUtils.isEMUI() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
//            editText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
//            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
//        }
//    }
//


    protected TitleBarView titleBarView;

    public void setLeftViewGone() {
        titleBarView = this.findViewById(R.id.titleBarView);
        titleBarView.setLeftVisible(View.INVISIBLE);
    }

    /**
     * 设置标题
     *
     * @param centerTxt
     */
    public void setViewTitle(String centerTxt) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);
    }

    /**
     * 设置标题及副标题
     *
     * @param centerTxt
     * @param subTitleTxt
     */
    public void setViewTitle(String centerTxt, String subTitleTxt) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);
        titleBarView.setCenterSubTxt(subTitleTxt);
    }

    /**
     * 设置标题,副标题，右侧文字及点击事件
     *
     * @param centerTxt
     * @param subTitleTxt
     * @param rightTxt
     * @param clickListener
     */
    public void setViewTitle(String centerTxt, String subTitleTxt, String rightTxt, View.OnClickListener clickListener) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);
        titleBarView.setCenterSubTxt(subTitleTxt);

        titleBarView.setRight1Txt(rightTxt, clickListener);
    }

    /**
     * 设置标题,副标题,右侧文字及点击事件,右侧2文字及点击2事件
     *
     * @param centerTxt
     * @param subTitleTxt
     * @param rightTxt
     * @param clickListener
     */
    public void setViewTitle(String centerTxt, String subTitleTxt, String rightTxt, View.OnClickListener clickListener, String right2Txt, View.OnClickListener click2Listener) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);
        if (!TextUtils.isEmpty(subTitleTxt)) {
            titleBarView.setCenterSubTxt(subTitleTxt);
        }
        titleBarView.setRight1Txt(rightTxt, clickListener);
        titleBarView.setRight2Txt(right2Txt, click2Listener);
    }

    /**
     * 设置标题,副标题,右侧文字及点击事件,右侧2文字及点击2事件
     *
     * @param centerTxt
     * @param rightTxt
     * @param clickListener
     * @param right2Txt
     * @param click2Listener
     */
    public void setViewTitle(String centerTxt, String rightTxt, View.OnClickListener clickListener, String right2Txt, View.OnClickListener click2Listener) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);

        titleBarView.setRight1Txt(rightTxt, clickListener);
        titleBarView.setRight2Txt(right2Txt, click2Listener);
    }

    /**
     * 设置标题,副标题，右侧图标及点击事件
     *
     * @param centerTxt
     * @param subTitleTxt
     * @param rightIcon
     * @param clickListener
     */
    public void setViewTitle(String centerTxt, String subTitleTxt, int rightIcon, View.OnClickListener clickListener) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);
        titleBarView.setCenterSubTxt(subTitleTxt);

        titleBarView.setRight1Icon(rightIcon, clickListener);
    }

    /**
     * 设置标题,副标题，右侧图标及点击事件
     *
     * @param centerTxt
     * @param subTitleTxt
     * @param rightIcon
     * @param clickListener
     */
    public void setViewTitle(String centerTxt, String subTitleTxt, int rightIcon, View.OnClickListener clickListener, int right2Icon, View.OnClickListener click2Listener) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);
        titleBarView.setCenterSubTxt(subTitleTxt);

        titleBarView.setRight1Icon(rightIcon, clickListener);
        titleBarView.setRight2Icon(right2Icon, click2Listener);
    }

    /**
     * 设置标题,副标题,右侧图标及点击事件,右侧2图标及点击2事件
     * @param centerTxt
     * @param rightIcon
     * @param clickListener
     * @param right2Icon
     * @param click2Listener
     */
    public void setViewTitle(String centerTxt, int rightIcon, View.OnClickListener clickListener, int right2Icon, View.OnClickListener click2Listener) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);

        titleBarView.setRight1Icon(rightIcon, clickListener);
        titleBarView.setRight2Icon(right2Icon, click2Listener);
    }

    /**
     * 设置标题,右侧图标及点击事件
     *
     * @param centerTxt
     * @param imgResId
     * @param clickListener
     */
    public void setViewTitle(String centerTxt, int imgResId, View.OnClickListener clickListener) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);

        titleBarView.setRight1Icon(imgResId, clickListener);
    }

    /**
     * 设置标题,右侧文字及点击事件
     *
     * @param centerTxt
     * @param rightTxt
     * @param clickListener
     */
    public void setViewTitle(String centerTxt, String rightTxt, View.OnClickListener clickListener) {
        titleBarView = this.findViewById(R.id.titleBarView);

        titleBarView.setLeftIcon(R.drawable.back_icon);
        titleBarView.setLeftVisible(View.VISIBLE);
        titleBarView.setLeftViewClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        titleBarView.setCenterTxt(centerTxt);

        titleBarView.setRight1Txt(rightTxt, clickListener);
    }


    // --------------
    public String stringFilterChinese(String str) {
        //只允许汉字
        String regEx = "[^\u4E00-\u9FA5]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.replaceAll("").trim();
    }

    public void setViewTitleVisibility(int visible) {
        if (titleBarView == null) {
            titleBarView = this.findViewById(R.id.titleBarView);
        }
        titleBarView.setVisibility(visible);
    }

}
