package cn.sinothk.hussars.parent;

import org.greenrobot.eventbus.EventBus;

/**
 * <pre>
 *  创建:  Liangyt 2018/12/9 on 14:46
 *  项目:  WuMinAndroid
 *  描述:
 *  更新:
 * <pre>
 */
public abstract class EventBusLoadRecycleViewTitleBaseActivity<T> extends LoadRecycleViewTitleBaseActivity<T> {

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public fun eventBusCallback(result: AppResultData<AppPageData<LawCaseEntity>>) {
//
//        if ("getHomeLawCasePageData" != result.eventType) {
//            return
//        }
//
//        stopLoading(loadingRecyclerView, loadType)
//
//        when (result.code) {
//            AppResultData.SUCCESS -> {
//                if (result.data != null) {
//                    setPageData(result.data)
//                } else {
//                    loadingTipView.showError("数据异常")
//                }
//            }
//            AppResultData.TOKEN_OVERDUE -> {
//                loadingTipView.showError("Token过期，请重新登录")
//            }
//            else -> {
//                loadingTipView.showError(result.msg)
//            }
//        }
//    }
//
//    private fun setPageData(appPageData: AppPageData<LawCaseEntity>) {
//        if (loadType == LoadType.REFRESH) {
//            if (appPageData.records.size == 0) {
//                loadingTipView.showEmpty("暂无数据", R.drawable.icon_no_data_gray)
//                loadingRecyclerView.setLoadingMoreEnabled(false)
//            } else {
//                adapter!!.setData(appPageData.records!!)
//                loadingTipView.showContent()
//            }
//        } else {
//            if (appPageData.records.size == 0) {
//                tip("没有更多数据了")
//            } else {
//                adapter!!.updateData(appPageData.records!!)
//                loadingTipView.showContent()
//            }
//        }
//
//        // 设置：通用
//        if (appPageData.current < appPageData.pages) {
//            loadingRecyclerView.setLoadingMoreEnabled(true)
//        } else {
//            loadingRecyclerView.setLoadingMoreEnabled(false)
//            loadingRecyclerView.setNoMore(true)
//        }
//    }
}
