package cn.sinothk.hussars.parent;

import android.content.Context;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.gyf.immersionbar.ImmersionBar;
import com.sinothk.android.utils.XUtils;
import com.sinothk.dialog.loading.LoadingDialog;

public abstract class BaseActivity extends AppCompatActivity {

    private LoadingDialog loadingDialog;

    public void tip(String msg) {
        XUtils.toast().show(msg);
    }

    public void showLoadingDialog(String msg) {
        loadingDialog = new LoadingDialog(this);
        loadingDialog.show(false, msg);
    }

    public void hideLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (loadingDialog != null) {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    public void softInputFocusable(EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();

        InputMethodManager inputManager = (InputMethodManager) editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert inputManager != null;
        inputManager.showSoftInput(editText, 0);
    }
}
