package cn.sinothk.hussars.parent;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.OrientationHelper;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.sinothk.android.utils.XUtils;
import com.sinothk.rxretrofit.bean.PageVo;
import com.sinothk.widget.loadingRecyclerView.LoadingRecyclerView;
import com.sinothk.widget.loadingRecyclerView.ProgressStyle;
import com.sinothk.widget.loadingRecyclerView.decorations.GridDecoration;
import com.sinothk.widget.loadingRecyclerView.decorations.StaggeredGridDecoration;

import cn.sinothk.hussars.R;

/**
 * <pre>
 *  创建:  Liangyt 2018/12/9 on 14:46
 *  项目:  WuMinAndroid
 *  描述:
 *  更新:
 * <pre>
 */
public abstract class LoadRecycleViewBaseFragment<T> extends BaseFragment {

    /**
     * =========================================
     * 刷新部分
     */
    private LoadingRecyclerView currRecyclerView;
    private int pageNo = 1;

    public enum LoadType {
        REFRESH, LOAD_MORE
    }

    protected LoadType loadType = LoadType.REFRESH;

    protected void initRecycleLinearView(LoadingRecyclerView recyclerView) {
        currRecyclerView = recyclerView;

        // ListView：设置方向
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(recyclerView.getListViewLine(getActivity(), getLineDrawable()));

        // 设置刷新样式
        recyclerView.setRefreshProgressStyle(ProgressStyle.BallScaleRippleMultiple);
        recyclerView.setLoadingMoreProgressStyle(ProgressStyle.BallScaleRippleMultiple);
        // 设置刷新样式:图标
//        recyclerView.setArrowImageView(R.drawable.iconfont_down_white_2);
        // 设置需要时间
        recyclerView.getDefaultRefreshHeaderView().setRefreshTimeVisible(true);
        // 设置加载更多相关信息
        recyclerView.getDefaultFootView().setLoadingHint("正在加载...");
        recyclerView.getDefaultFootView().setNoMoreHint("已全部加载");
        recyclerView.setLimitNumberToCallLoadMore(2);

        recyclerView.setLoadingListener(new LoadingRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }

            @Override
            public void onLoadMore() {
                loadMoreData();
            }
        });
    }

    protected int getLineDrawable() {
        return R.drawable.list_divider;
    }

    protected void initRecycleGridView(LoadingRecyclerView recyclerView, int lineSize, int columnCount, int headerCount) {
        currRecyclerView = recyclerView;

        // 网格
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), columnCount, GridLayoutManager.VERTICAL, false));
//        recyclerView.addItemDecoration(recyclerView.getGridViewLine(10)); //
        recyclerView.addItemDecoration(new GridDecoration(lineSize, columnCount, headerCount));

        // 设置刷新样式
        recyclerView.setRefreshProgressStyle(ProgressStyle.BallScaleRippleMultiple);
        recyclerView.setLoadingMoreProgressStyle(ProgressStyle.BallScaleRippleMultiple);

        // 设置刷新样式:图标
        recyclerView.setArrowImageView(R.drawable.iconfont_down_white_2);
        // 设置需要时间
        recyclerView.getDefaultRefreshHeaderView().setRefreshTimeVisible(true);
        // 设置加载更多相关信息
        recyclerView.getDefaultFootView().setLoadingHint("正在加载...");
        recyclerView.getDefaultFootView().setNoMoreHint("已全部加载");
        recyclerView.setLimitNumberToCallLoadMore(2);

        recyclerView.setLoadingListener(new LoadingRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }

            @Override
            public void onLoadMore() {
                loadMoreData();
            }
        });
    }

    protected void initRecycleStaggeredGridView(LoadingRecyclerView recyclerView, int lineSize, int columnCount, int headerCount) {
        currRecyclerView = recyclerView;

        // 设置布局管理器/初始化布局管理器
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(columnCount, OrientationHelper.VERTICAL);
        
//        //防止Item切换 2021年12月2日 00:06:51 未测试过
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        currRecyclerView.setItemAnimator(null);

        currRecyclerView.setLayoutManager(layoutManager);
        // 设置间隔样式
        currRecyclerView.addItemDecoration(new StaggeredGridDecoration(lineSize, columnCount, headerCount));

        // 设置刷新样式
        recyclerView.setRefreshProgressStyle(ProgressStyle.BallScaleRippleMultiple);
        recyclerView.setLoadingMoreProgressStyle(ProgressStyle.BallScaleRippleMultiple);
        // 设置刷新样式:图标
        recyclerView.setArrowImageView(R.drawable.iconfont_down_white_2);
        // 设置需要时间
        recyclerView.getDefaultRefreshHeaderView().setRefreshTimeVisible(true);
        // 设置加载更多相关信息
        recyclerView.getDefaultFootView().setLoadingHint("正在加载...");
        recyclerView.getDefaultFootView().setNoMoreHint("已全部加载");
        recyclerView.setLimitNumberToCallLoadMore(2);

        recyclerView.setLoadingListener(new LoadingRecyclerView.LoadingListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }

            @Override
            public void onLoadMore() {
                loadMoreData();
            }
        });
    }


    protected void stopLoading(LoadingRecyclerView recyclerView, LoadType loadType) {
        if (recyclerView == null) {
            return;
        }

        if (loadType == LoadType.REFRESH) {
            recyclerView.refreshComplete();
        } else {
            recyclerView.loadMoreComplete();
        }
    }

    private PageVo<T> getInitPageVo(int pageNo) {
        PageVo<T> pageVo = new PageVo<>();
        pageVo.setPageNum(pageNo);
        pageVo.setPageSize(20);
        return pageVo;
    }

    public void refreshData() {
        if (!XUtils.net().isConnected()) {
            XUtils.toast().show("当前网络不可用");
            stopLoading(currRecyclerView, LoadType.REFRESH);
            return;
        }

        loadType = LoadType.REFRESH;

        pageNo = 1;
        loadData(getInitPageVo(pageNo));
    }

    void loadMoreData() {
        if (!XUtils.net().isConnected()) {
            XUtils.toast().show("当前网络不可用");
            stopLoading(currRecyclerView, LoadType.LOAD_MORE);
            return;
        }

        loadType = LoadType.LOAD_MORE;
        loadData(getInitPageVo(++pageNo));
    }

    public abstract void loadData(PageVo<T> pageVo);

//    @Override
//    protected void doRetry() {
//        refreshData();
//    }
}
