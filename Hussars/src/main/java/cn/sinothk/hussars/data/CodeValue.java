package cn.sinothk.hussars.data;

public class CodeValue {
    public static final int ResultCodeSuccess = 200;
    public static final String Data = "data";

    public static final int OrgTownSelectCode = 300;
    public static final int IMG_SELECT_CODE = 301;
    public static final int MAP_SELECT_POI_CODE = 302;
    public static final int IMG_CROP_CODE = 303;
}
