package cn.sinothk.hussars.three.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import com.sinothk.android.utils.XUtils;

import java.io.File;

public class ImageViewGlide {

//    Glide.with(context)
//            .load(url)
//    //如果传入的url是http://..... .gif(尾缀是.gif)
//    //需要添加 .asBitmap() 方法处理,其他格式的图片不需要添加
//				.asBitmap()
//				.placeholder(R.drawable.default)
//		        .dontAnimate()
//		        .into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
//        @Override
//        public void onResourceReady(Bitmap resource, GlideAnimation glideAnimation) {
//            int imageHeight = resource.getHeight();
//            if(imageHeight > 4096) {
//                imageHeight = 4096;
//                ViewGroup.LayoutParams para = imageview.getLayoutParams();
//                para.width = LayoutParams.MATCH_PARENT;
//                para.height = imageHeight;
//                imageview.setLayoutParams(para);
//
//                Glide.with(context)
//                        .load(url)
//                        .placeholder(R.drawable.default)
//							.dontAnimate()
//                            .centerCrop()
//                            .into(imageview);
//            }
//            else {
//                Glide.with(context)
//                        .load(url)
//                        .placeholder(R.drawable.default)
//							.dontAnimate()
//                            .into(imageview);
//            }
//        }
//    });

    public static void show(Context applicationContext, String url, ImageView imageView, int errorResId) {
        if (XUtils.string().isNotEmpty(url)) {
            Glide.with(applicationContext)
                    .load(url)
                    .placeholder(errorResId)
                    .error(errorResId)
                    .skipMemoryCache(true)                      //禁止Glide内存缓存
                    .diskCacheStrategy(DiskCacheStrategy.NONE)  //不缓存资源
                    .into(imageView);
        } else {
            imageView.setImageResource(errorResId);
        }
    }

    public static void show(Context applicationContext, String url, ImageView imageView, int errorResId, int size) {
        if (XUtils.string().isNotEmpty(url)) {
            Glide.with(applicationContext)
                    .load(url)
                    .placeholder(errorResId)
                    .error(errorResId)
                    .skipMemoryCache(true)                      //禁止Glide内存缓存
                    .diskCacheStrategy(DiskCacheStrategy.NONE)  //不缓存资源
                    .override(size)
                    .into(imageView);
        } else {
            imageView.setImageResource(errorResId);
        }
    }

    public static void show(Context applicationContext, String url, ImageView imageView, int errorResId, int width, int height) {
        if (XUtils.string().isNotEmpty(url)) {
            Glide.with(applicationContext)
                    .load(url)
                    .placeholder(errorResId)
                    .error(errorResId)
                    .skipMemoryCache(true)                      //禁止Glide内存缓存
                    .diskCacheStrategy(DiskCacheStrategy.NONE)  //不缓存资源
                    .override(width, height)
                    .into(imageView);
        } else {
            imageView.setImageResource(errorResId);
        }
    }

    public static void show(Context applicationContext, File file, ImageView imageView, int errorResId) {
        if (file != null) {
            Glide.with(applicationContext)
                    .load(file)
                    .placeholder(errorResId)
                    .error(errorResId)
                    .skipMemoryCache(true)                      //禁止Glide内存缓存
                    .diskCacheStrategy(DiskCacheStrategy.NONE)  //不缓存资源
                    .into(imageView);
        } else {
            imageView.setImageResource(errorResId);
        }
    }

    public static void show(Context applicationContext, File file, ImageView imageView, int errorResId, int size) {
        if (file != null) {
            Glide.with(applicationContext)
                    .load(file)
                    .placeholder(errorResId)
                    .error(errorResId)
                    .skipMemoryCache(true)                      //禁止Glide内存缓存
                    .diskCacheStrategy(DiskCacheStrategy.NONE)  //不缓存资源
                    .override(size)
                    .into(imageView);
        } else {
            imageView.setImageResource(errorResId);
        }
    }

    public static void show(Context applicationContext, File file, ImageView imageView, int errorResId, int width, int height) {
        if (file != null) {
            Glide.with(applicationContext)
                    .load(file)
                    .placeholder(errorResId)
                    .error(errorResId)
                    .skipMemoryCache(true)                      //禁止Glide内存缓存
                    .diskCacheStrategy(DiskCacheStrategy.NONE)  //不缓存资源
                    .override(width, height)
                    .into(imageView);
        } else {
            imageView.setImageResource(errorResId);
        }
    }

    public static void showForBitmap(final Context applicationContext, String url, final Target<Bitmap> target) {
        if (XUtils.string().isNotEmpty(url)) {
            Glide.with(applicationContext)
                    .asBitmap()
                    .load(url)
                    .into(target);
        }
    }


}
