package cn.sinothk.hussars.views.scrollViewGridView;

public class IconTextBean {

    private int iconResId;
    private String text;

    public IconTextBean() {
    }

    public IconTextBean(int iconResId, String text) {
        this.iconResId = iconResId;
        this.text = text;
    }

    public int getIconResId() {
        return iconResId;
    }

    public void setIconResId(int iconResId) {
        this.iconResId = iconResId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
