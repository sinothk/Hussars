//package cn.sinothk.hussars.views.webView
//
//import android.annotation.SuppressLint
//import android.app.Activity
//import android.content.Intent
//import android.net.http.SslError
//import android.os.Bundle
//import android.view.KeyEvent
//import android.view.View
//import android.webkit.*
//import cn.sinothk.hussars.R
//import cn.sinothk.hussars.parent.TitleBaseActivity
//import com.sinothk.android.utils.XUtils
//import kotlinx.android.synthetic.main.comm_activity_web_details.*
//
//class CommWebDetailsActivity : TitleBaseActivity() {
//
//    private var url = ""
//    private var titleTxt: String? = null
//
//    companion object {
//
//        @JvmStatic
//        fun start(activity: Activity?, bundle: Bundle) {
//            val intent = Intent(activity, CommWebDetailsActivity::class.java)
//            intent.putExtras(bundle)
//            activity?.startActivity(intent)
//        }
//
//        fun start(activity: Activity?, titleTxt: String, url: String) {
//            val bundle = Bundle()
//            bundle.putString("titleTxt", titleTxt)
//            bundle.putString("url", url)
//            val intent = Intent(activity, CommWebDetailsActivity::class.java)
//            intent.putExtras(bundle)
//            activity?.startActivity(intent)
//        }
//    }
//
//    override fun getLayoutResId(): Int = R.layout.comm_activity_web_details
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//
//        titleTxt = intent.getStringExtra("titleTxt")
//        url = intent.getStringExtra("url") as String
//
//        setViewTitle(XUtils.string().getNotNullValue(titleTxt, "详情"))
//        titleBarView.setCenterTxtColor(R.color.page_title)
//
//        setView(url)
//    }
//
//    @SuppressLint("SetJavaScriptEnabled", "SetTextI18n")
//    private fun setView(url: String?) {
//
//        if (url == null) {
//            return
//        }
//
//        val webSettings = webView.settings//获取webview设置属性
//        webSettings.javaScriptEnabled = true//支持js
//        webSettings.blockNetworkImage = false;//解决图片不显示
//        webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
//        webView.loadUrl(url)
//
//        webView.webChromeClient = object : WebChromeClient() {
//            override fun onProgressChanged(view: WebView, newProgress: Int) {
//                super.onProgressChanged(view, newProgress)
//                if (newProgress < 100) {
//                    progressView.visibility = View.VISIBLE
//                    progressTv.text = "$newProgress %"
//                } else {
//                    progressView.visibility = View.GONE
//                }
//            }
//        }
//
//        webView.webViewClient = object : WebViewClient() {
//            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
////                // 为了让内容显示不跳到默认浏览器重写此方法
////                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////                    view.loadUrl(url)
////                    return true
////                }
//                return false
//            }
//
//            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
//                return false
//            }
//
//            override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
//                handler?.proceed()
////                super.onReceivedSslError(view, handler, error)
//            }
//        }
//    }
//
////    override fun finish() {
////        if (webView.canGoBack()) {
////            webView.goBack()
////            return
////        }
////        super.finish()
////    }
//
//    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
//        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
//            webView.goBack()
//            return true
//        }
//        return super.onKeyDown(keyCode, event)
//    }
//}
