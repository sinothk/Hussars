package cn.sinothk.hussars.views.scrollViewGridView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinothk.android.utils.inters.OnSuperListener;

import java.util.ArrayList;

import cn.sinothk.hussars.R;

public class FunItemAdapter extends BaseAdapter {
    private OnSuperListener<IconTextBean> superListener;
    private Context mContext;
    private ArrayList<IconTextBean> listData;

    public FunItemAdapter(Context mContext) {
        this.mContext = mContext;
        listData = new ArrayList<>();
    }

    public FunItemAdapter(Context mContext, ArrayList<IconTextBean> listData) {
        this.mContext = mContext;
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public IconTextBean getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

//        if (convertView == null) {
//            convertView = LayoutInflater.from(mContext).inflate(R.layout.comm_fun_list_item, parent, false);
//        } else {
//
//        }

        final IconTextBean bean = listData.get(position);

        convertView = LayoutInflater.from(mContext).inflate(R.layout.androidx_grid_view_fun_list_item, parent, false);

        LinearLayout rootView = convertView.findViewById(R.id.rootView);
        ImageView iconIv = convertView.findViewById(R.id.iconIv);
        TextView textView = convertView.findViewById(R.id.textView);

        iconIv.setImageResource(bean.getIconResId());
        textView.setText(bean.getText());

        if (superListener != null) {
            rootView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    superListener.onClick(position, bean, "rootView");
                }
            });
        }
        return convertView;
    }

    public void setListData(ArrayList<IconTextBean> newListData) {

        if (newListData == null) {
            return;
        }
        listData = (ArrayList<IconTextBean>) newListData.clone();
    }

    public void setOnItemClickListener(OnSuperListener<IconTextBean> superListener) {
        this.superListener = superListener;
    }
}
