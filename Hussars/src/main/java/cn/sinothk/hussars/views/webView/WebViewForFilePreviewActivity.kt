//package cn.sinothk.hussars.views.webView
//
//import android.annotation.SuppressLint
//import android.app.Activity
//import android.content.Intent
//import android.os.Bundle
//import android.view.View
//import android.webkit.WebChromeClient
//import android.webkit.WebResourceRequest
//import android.webkit.WebView
//import android.webkit.WebViewClient
//import cn.sinothk.hussars.R
//import cn.sinothk.hussars.parent.TitleBaseActivity
//import com.sinothk.android.utils.XUtils
//import kotlinx.android.synthetic.main.web_view_file_preview.*
//
//
//open class WebViewForFilePreviewActivity : TitleBaseActivity() {
//    var rootWebView: ScrollWebView? = null
//    private var url = ""
//    private var titleTxt: String? = null
//
//    companion object {
//
//        @JvmStatic
//        fun start(activity: Activity?, bundle: Bundle) {
//            val intent = Intent(activity, WebViewForFilePreviewActivity::class.java)
//            intent.putExtras(bundle)
//            activity?.startActivity(intent)
//        }
//
//        fun start(activity: Activity?, titleTxt: String, url: String) {
//            val bundle = Bundle()
//            bundle.putString("titleTxt", titleTxt)
//            bundle.putString("url", url)
//            val intent = Intent(activity, WebViewForFilePreviewActivity::class.java)
//            intent.putExtras(bundle)
//            activity?.startActivity(intent)
//        }
//    }
//
//    override fun getLayoutResId(): Int = R.layout.web_view_file_preview
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//
//        titleTxt = intent.getStringExtra("titleTxt")
//        url = intent.getStringExtra("url") as String
//
//        setViewTitle(XUtils.string().getNotNullValue(titleTxt, "详情"))
//        titleBarView.setCenterTxtColor(R.color.page_title)
//
//        setView(url)
//    }
//
//    @SuppressLint("SetJavaScriptEnabled", "SetTextI18n", "LongLogTag")
//    private fun setView(url: String?) {
//
//        if (url == null) {
//            return
//        }
//
//        rootWebView = webView
//
//        val webSettings = webView.settings//获取webview设置属性
//        webSettings.javaScriptEnabled = true//支持js
//        // 缩放
//        webSettings.setSupportZoom(true)
//        webSettings.builtInZoomControls = true
//        webSettings.displayZoomControls = true
//        // 自适应
//        webSettings.useWideViewPort = true;
//        webSettings.loadWithOverviewMode = true;
//
//        webView.loadUrl(url)
//
//        webView.webChromeClient = object : WebChromeClient() {
//            override fun onProgressChanged(view: WebView, newProgress: Int) {
//                super.onProgressChanged(view, newProgress)
//                if (newProgress < 100) {
//                    progressView.visibility = View.VISIBLE
//                    progressTv.text = "$newProgress %"
//                } else {
//                    progressView.visibility = View.GONE
//                }
//            }
//        }
//
//        webView.webViewClient = object : WebViewClient() {
//            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
////                // 为了让内容显示不跳到默认浏览器重写此方法
////                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
////                    view.loadUrl(url)
////                    return true
////                }
//                return false
//            }
//
//            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
//                return false
//            }
//
//            override fun onPageFinished(view: WebView, url: String?) {
//                super.onPageFinished(view, url)
//            }
//        }
//
////        webView.setOnScrollEventListener(object : ScrollWebView.OnScrollEventListener {
////            override fun onPageTop(l: Int, t: Int, oldl: Int, oldt: Int) {
//////                Log.e("setOnScrollChangeListener", "onPageTop")
////            }
////
////            override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
//////                Log.e("setOnScrollChangeListener", "l = $l, t = $t, oldl=$oldl, oldt = $oldt")
////            }
////
////            override fun onPageBottom(l: Int, t: Int, oldl: Int, oldt: Int) {
////                Log.e("setOnScrollChangeListener", "isBottom => 学习完了")
////            }
////        })
//    }
//}
