package cn.sinothk.hussars.views.webView;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;

public class ScrollWebView extends WebView {

    private OnScrollEventListener scrollChangeListener;

    public ScrollWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        // webview的高度
        float webcontent = getContentHeight() * getScale();
        // 当前webview的高度
        float webnow = getHeight() + getScrollY();
        if (Math.abs(webcontent - webnow) < 1) {
            //处于底端
            scrollChangeListener.onPageBottom(l, t, oldl, oldt);
        } else if (getScrollY() == 0) {
            //处于顶端
            scrollChangeListener.onPageTop(l, t, oldl, oldt);
        } else {
            scrollChangeListener.onScrollChanged(l, t, oldl, oldt);
        }
    }

    public void setOnScrollEventListener(OnScrollEventListener scrollChangeListener) {
        this.scrollChangeListener = scrollChangeListener;
    }

    public interface OnScrollEventListener {

        void onPageTop(int l, int t, int oldl, int oldt);

        void onScrollChanged(int l, int t, int oldl, int oldt);

        void onPageBottom(int l, int t, int oldl, int oldt);

    }
}
