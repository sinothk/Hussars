package cn.sinothk.hussars.base;

import android.os.Bundle;
import cn.sinothk.hussars.parent.TitleBaseActivity;

public abstract class WelcomeBaseActivity extends TitleBaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initTitleBarFullStyle();
    }
}