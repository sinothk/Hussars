package com.sinothk.amap.choose.demo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.amap.api.location.AMapLocationClient;
import com.sinothk.amap.choose.LocationHelper;
import com.sinothk.amap.choose.SearchMainActivity;
import com.sinothk.amap.choose.bean.ChoosePoiItemBean;
import com.sinothk.amap.choose.bean.LocationBean;
import com.sinothk.amap.choose.inters.LocationCallback;

import cn.sinothk.hussars.demo.R;

public class AMapMainDemoActivity extends AppCompatActivity {

    private Button doSelectBtn, stopLocateBtn;
    private TextView addressTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.amap_activity_main_demo);

        doSelectBtn = this.findViewById(R.id.doSelectBtn);
        stopLocateBtn = this.findViewById(R.id.stopLocateBtn);
        addressTv = this.findViewById(R.id.addressTv);

        AMapLocationClient.updatePrivacyAgree(this, true);
        AMapLocationClient.updatePrivacyShow(this, true, true);


        doSelectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getApplicationContext(), SearchMainActivity.class);
                myIntent.putExtra("id", "123456789");
                startActivityForResult(myIntent, 300);
            }
        });

        stopLocateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationHelper.obj().onDestroy();
            }
        });


        try {
            LocationHelper.obj().singleLocation(this, new LocationCallback() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onLocation(LocationBean location) {
                    if (location != null) {
                        String str = addressTv.getText().toString();
                        addressTv.setText(str + "\n" + location.getLatitude());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationHelper.obj().onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != 200 || data == null) {
            return;
        }

        if (requestCode == 300) {
            ChoosePoiItemBean choosePoiItem = (ChoosePoiItemBean) data.getSerializableExtra("choosePoiItem");
            if (choosePoiItem != null) {
                addressTv.setText(choosePoiItem.toString());
            }
        }
    }

    //    @SuppressLint("SetTextI18n")
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main_demo)
//
////        AMapLocationClient.updatePrivacyAgree(this.applicationContext, true);
////        AMapLocationClient.updatePrivacyShow(this.applicationContext, true, true);
////
////        doSelectBtn.setOnClickListener {
////            val myIntent = Intent(this@MainDemoActivity, SearchMainActivity::class.java)
////            myIntent.putExtra("id","123456789")
////            startActivityForResult(myIntent, 300)
////        }
////

////
////        stopLocateBtn.setOnClickListener {
////            LocationHelper.obj().onDestroy()
////        }
//    }

//    override fun onDestroy() {
//        super.onDestroy()
//        LocationHelper.obj().onDestroy();
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (resultCode != 200 || data == null) {
//            return
//        }
//
//        when (requestCode) {
//            300 -> {
//                val choosePoiItem: ChoosePoiItemBean = data.getSerializableExtra("choosePoiItem") as ChoosePoiItemBean
//
//            }
//        }
//    }
}