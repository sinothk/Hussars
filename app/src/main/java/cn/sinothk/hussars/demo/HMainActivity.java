package cn.sinothk.hussars.demo;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.sinothk.android.utils.XUtils;

import cn.sinothk.hussars.parent.TitleBaseActivity;


public class HMainActivity extends TitleBaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setViewTitle("按分销售", "订单", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HMainActivity.this, "订单", Toast.LENGTH_SHORT).show();
            }
        }, "管理", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HMainActivity.this, "管理", Toast.LENGTH_SHORT).show();
            }
        });

//        final ImageView imageView = findViewById(R.id.imageView);
////        ImageViewGlide.show(getApplicationContext(), "", imageView, R.mipmap.ic_launcher);
//
//        ImageViewGlide.showForBitmap(getApplicationContext(), "https://upload-images.jianshu.io/upload_images/6417733-c9834375d26eb500.PNG?imageMogr2/auto-orient/strip|imageView2/2/w/930/format/webp", new CustomTarget<Bitmap>() {
//
//            @Override
//            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                imageView.setImageBitmap(resource);
//            }
//
//            @Override
//            public void onLoadCleared(@Nullable Drawable placeholder) {
//            }
//        });
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.h_activity_main;
    }

//    @Override
//    public int getLayoutResId() {
//        return R.layout.h_activity_main;
//    }
}